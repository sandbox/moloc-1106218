<?php
// $Id$

/**
 * @file
 * Content Type, used by the Memory of EthifyGame
 */


function ethifygame_level_memory_get_content_type_spielkarte() {
  $content = array();
  
  $content['type']  = array (
    'name' => 'Spielkarte',
    'type' => 'spielkarte',
    'description' => 'Spielkarte für das Ethify Game',
    'title_label' => 'Title',
    'body_label' => 'Body',
    'min_word_count' => '0',
    'help' => '',
  
    'node_options' => 
    array (
      'status' => false,
      'promote' => false,
      'sticky' => false,
      'revision' => false,
    ),
    'language_content_type' => '0',
    'upload' => '1',
    'show_preview_changes' => 1,
    'show_diff_inline' => 0,
    'enable_revisions_page' => 1,
  
    'forward_display' => 1,
    'trash_settings' => 
    array (
      'enabled' => false,
      'move_to_trash_tab' => false,
      'remove_delete_button' => false,
    ),
    'community_tags_display' => '1',
    'old_type' => 'spielkarte',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'og_content_type_usage' => 'omitted',
    'og_max_groups' => '',
    'signup_node_default_state' => 'disabled',
    'signup_date_field' => '0',
    'content_profile_use' => 0,
    'comment' => '2',
    'comment_default_mode' => '4',
    'comment_default_order' => '1',
    'comment_default_per_page' => '50',
    'comment_controls' => '3',
    'comment_anonymous' => 0,
    'comment_subject_field' => '1',
    'comment_preview' => '1',
    'comment_form_location' => '0',
    'comment_upload' => '0',
    'comment_upload_images' => 'none',
    'onoff' => 0,
    'print_display' => 1,
    'print_display_comment' => 0,
    'print_display_urllist' => 1,
    'print_mail_display' => 1,
    'print_mail_display_comment' => 0,
    'print_mail_display_urllist' => 1,
    'print_pdf_display' => 1,
    'print_pdf_display_comment' => 0,
    'print_pdf_display_urllist' => 1,
    'skinr_settings' => 
    array (
      'comment_group' => 
      array (
        'ethify_mobile_garland' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
      'node_group' => 
      array (
        'ethify_mobile_garland' => 
        array (
          'advanced' => 
          array (
            '_additional' => '',
            '_template' => '',
          ),
        ),
      ),
    ),
    'tableofcontents_toc_automatic' => '0',
    'tableofcontents_vtoc' => 0,
    'tableofcontents_remove_from_teaser' => 1,
    'ant' => '0',
    'ant_pattern' => '',
  
    'auto_nodetitle_php' => '',
    'i18n_newnode_current' => 0,
    'i18n_required_node' => 0,
    'i18n_lock_node' => 0,
    'i18n_node' => 1,
    'notifications_content_type' => 
    array (
      'grouptype' => true,
      'thread' => true,
      'author' => true,
      'nodetype' => false,
      'typeauthor' => false,
    ),
    'icl_content_node_type_fields' => 
    array (
  /* title and body can not be imported.
      'title' => true,
      'body' => true, */
      'log' => false,
      'name' => false,
      'date' => false,
      'link_title' => false,
      'field_content' => false,
    ),
  );
  $content['fields']  = array (
    0 => 
    array (
      'label' => 'Game Card',
      'field_name' => 'field_cardimage',
      'type' => 'filefield',
      'widget_type' => 'imagefield_widget',
      'change' => 'Change basic information',
      'weight' => '31',
      'file_extensions' => 'png gif jpg jpeg',
      'progress_indicator' => 'bar',
      'file_path' => '',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => 0,
      'min_resolution' => 0,
      'custom_alt' => 0,
      'alt' => '',
      'custom_title' => 0,
      'title_type' => 'textfield',
      'title' => '',
      'use_default_image' => 0,
      'default_image_upload' => '',
      'default_image' => NULL,
      'description' => '',
      'group' => false,
      'required' => 0,
      'multiple' => '0',
      'list_field' => '0',
      'list_default' => 1,
      'description_field' => '0',
      'op' => 'Save field settings',
      'module' => 'filefield',
      'widget_module' => 'imagefield',
      'columns' => 
      array (
        'fid' => 
        array (
          'type' => 'int',
          'not null' => false,
          'views' => true,
        ),
        'list' => 
        array (
          'type' => 'int',
          'size' => 'tiny',
          'not null' => false,
          'views' => true,
        ),
        'data' => 
        array (
          'type' => 'text',
          'serialize' => true,
          'views' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        5 => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        2 => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        3 => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
      ),
    ),
    1 => 
    array (
      'label' => 'Game Card Content',
      'field_name' => 'field_content',
      'type' => 'nodereference',
      'widget_type' => 'nodereference_url',
      'change' => 'Change basic information',
      'weight' => '32',
      'fallback' => 'autocomplete',
      'edit_fallback' => 1,
      'node_link' => 
      array (
        'teaser' => 0,
        'full' => 0,
        'title' => '',
        'hover_title' => '',
        'destination' => 'default',
      ),
      'description' => '',
      'group' => false,
      'required' => 0,
      'multiple' => '0',
      'referenceable_types' => 
      array (
        'page' => 'page',
      ),
      'advanced_view' => '--',
      'advanced_view_args' => '',
      'op' => 'Save field settings',
      'module' => 'nodereference',
      'widget_module' => 'nodereference_url',
      'columns' => 
      array (
        'nid' => 
        array (
          'type' => 'int',
          'unsigned' => true,
          'not null' => false,
          'index' => true,
        ),
      ),
      'display_settings' => 
      array (
        'label' => 
        array (
          'format' => 'above',
          'exclude' => 0,
        ),
        5 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'teaser' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        2 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        3 => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'body_field' => '0',
    'revision_information' => '20',
    'author' => '20',
    'options' => '25',
    'comment_settings' => '30',
    'menu' => '-2',
    'book' => '10',
    'path' => '30',
    'attachments' => '30',
    'domain' => '1',
    'print' => '30',
  );
  
  return $content;
}