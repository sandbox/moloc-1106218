// $Id$

The Memory-Level module of the EthifyGame provides a memory-card view with a 
default image. Clicking a card will open a lighbox with a node-content,
which can be flagged with either "Done", "ToDo" or "Ignore". Once one of these
Buttons are clicked, the memory-card is marked as played and you will see a
specific image on the card. If enough cards are played, the next level will
be activated.

The Memory-Level will throw an event, either if the player hit one of the
buttons "Done", "ToDo" or "Ignore", or if the level is completed. Therefore
other modules can react on these events.

Functionality:
 + multiple memory-cards (own content-type)
 + a view with random memory-cards
 + executes an event "level done"
 + executes an event "Card Done", "Card ToDo" or "Card Ignore"
 + Debug mode for administrator (flag/unflag cards)
 + Configureable amount of cards to play to finish level
 
Requirements
------------
 + Drupal 6
 + Modules
   - ethifygame
   - view
   - flag
   - content
   - rules (optional)

Installation
------------
1) Copy the Memory directory to the modules folder in your installation.

2) Copy the *.tpl.php files from the theme folder of the Memory module
   to the theme folder of your site (sites\all\themes\acquia_prosper)

3) Enable the module using Administer -> Modules (/admin/build/modules)
   (Note: EthifyGame Module must be installed/enabled first!)

Configuration
-------------

1) Configure default settings for the level
   Administer -> Site Configuration -> Ethifygame -> Memory
   (/admin/settings/ethifygame/level/memory)

2) Add Triggered Rules, if needed (e.g. give ethify points on level done)
   Administer -> Rules -> Triggered rules (admin/rules/trigger)

2) Create Memory Cards Administer -> Content management -> Create Content ->
   Spielkarte (/node/add/spielkarte)

Using the module
----------------

After enabling the module, the memory level is active and can be played.
Ethifygame will automatically set the level-number on the first time, this
level is enabled, as the last level. You can modify the level-order in the
ethifygame settings (see /admin/settings/ethifygame).