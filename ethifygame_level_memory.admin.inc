<?php
// $Id$

/**
 * @file
 * Callback admin settings form function of the eg-memory module.
 */

/**
 * Builds the form for configuring the settings
 * of the memory module.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function _ethifygame_level_memory_admin_settings() {

  $form['ethifygame_level_memory_cards_needed_for_next_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of cards to play'),
    '#description' => t('Count of cards, the user has to play, until level is marked as "done".'),
    '#default_value' => variable_get('ethifygame_level_memory_cards_needed_for_next_level', 9),
  );
  $form['ethifygame_level_memory_debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug Mode'),
    '#description' => t('Provides extra links in the memory view to set flag-state of a game card.'),
    '#default_value' => variable_get('ethifygame_level_memory_debug_mode', 0),
  );
  return system_settings_form($form);
}

function _ethifygame_level_memory_admin_settings_validate($form, $form_state) {

  $card_count = $form['#post']['ethifygame_level_memory_cards_needed_for_next_level'];

  if( !is_numeric($card_count) || ((string)intval($card_count)) != $card_count ) {
    form_set_error('ethifygame_level_memory_cards_needed_for_next_level',
        t('Count of cards need to be decimal number.'));
  }
}
