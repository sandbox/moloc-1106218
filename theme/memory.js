// $Id$

if( typeof(EthifyGame) == typeof(undefined) ) {
  EthifyGame = {};
  EthifyGame.Level = {};
}

EthifyGame.Level.Memory = {};

$(document).ready(function() {
  var last_nid = null;
  var btn = {
      done: null,
      todo: null,
      ignore: null
  }; 

  var levelNavigationBuild = false;
  var levelNavigationPrevBuild = false;
  var levelNavigationNextBuild = false;
  
  function PlayCard(nid) {
    last_nid = nid;
    
    // check, whether the card is already played...
    if( $('#eg-card-default-' + nid).hasClass("eg-card-default-image") ) {
      btn.done.show();
      btn.todo.show();
      btn.ignore.show();
    } else {
      btn.done.hide();
      btn.todo.hide();
      btn.ignore.hide();
    }
  }
  
  function Init() {
    // lightbox
    var btnstr = '';
    btnstr += '<span id="eg-level-buttons">';
    btnstr += '<a id="eg-level-memory-done"></a>';
    btnstr += '<a id="eg-level-memory-todo"></a>';
    btnstr += '<a id="eg-level-memory-ignore"></a>';
    btnstr += '</span>';
    
    $('#bottomNav').prepend(btnstr);
    $('#bottomNav').addClass('content-group-inner');
    
    btn.done = $('#eg-level-memory-done');
    btn.todo = $('#eg-level-memory-todo');
    btn.ignore = $('#eg-level-memory-ignore');
    
    btn.done.click(function(event) {
      event.preventDefault();
      OnCardActionClick(last_nid, 'done');
    });
    btn.todo.click(function(event) {
      event.preventDefault();
      OnCardActionClick(last_nid, 'todo');
    });
    btn.ignore.click(function(event) {
      event.preventDefault();
      OnCardActionClick(last_nid, 'ignore');
    });
    
    if (!!$('input[name=eg-card-nav-prev]').val()) {
      LevelNavigationShow('prev');
    }
    if (!!$('input[name=eg-card-nav-next]').val()) {
      LevelNavigationShow('next');
    }
    
    // reorder lighbox buttons (from bottom to top)
    $('#imageDataContainer').insertBefore('#outerImageContainer');
  }
  
  function OnCardActionClick(nid, action) {
    // display the card
    CardShow(nid, true);
    
    // create solved url
    var solved_url = $('input[name=eg-card-solved-' + nid + '-' + action + '-link]').val();
    // we are not interested on success/error
    $.ajax({
      url: solved_url,
      dataType: "json",
      success: function(data, textStatus, jqXHR) {
        if (data.play_next_level) {
          LevelNavigationShow('next');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
    	  CardShow(nid, false);
      }
    });
  }
  
  function CardShow(nid, show) {
    var el_default = $("#eg-card-default-" + nid);
    var el_image = $("#eg-card-image-" + nid);
    
    if( show ) {
      el_image.removeClass("eg-card-hide");
      el_default.removeClass("eg-card-default-image");
    } else {
      el_default.addClass("eg-card-default-image");
      el_image.addClass("eg-card-hide");
    }
  }
  
  function LevelNavigationShow(action) {
    if (!levelNavigationBuild) {
      levelNavigationBuild = true;
      $('div.view-id-eg_memory div.view-content > table').css('clear', 'both');
    }
    if (action=='prev' && !levelNavigationPrevBuild) {
      levelNavigationPrevBuild = true;
      var rootEl = $('div.view-id-eg_memory div.view-content');
      rootEl.prepend($('input[name=eg-card-level-prev-link]').val());
    }
    if (action=='next' && !levelNavigationNextBuild) {
      levelNavigationNextBuild = true;
      var rootEl = $('div.view-id-eg_memory div.view-content');
      rootEl.prepend($('input[name=eg-card-level-next-link]').val());
    }
  }
  
  Init();
  EthifyGame.Level.Memory.PlayCard = PlayCard;
  EthifyGame.Level.Memory.OnCardActionClick = OnCardActionClick;
  EthifyGame.Level.Memory.CardShow = CardShow;
  EthifyGame.Level.Memory.LevelNavigationShow = LevelNavigationShow;
});
