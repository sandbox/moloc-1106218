<?php
// $Id: views-view-fields.tpl.php,v 1.6 2008/09/24 22:48:21 merlinofchaos Exp $
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
  if ($eg_lvl_memory['setup']) {
    drupal_add_css(drupal_get_path('module', 'ethifygame_level_memory') .'/theme/memory.css');
    drupal_add_js(drupal_get_path('module', 'ethifygame_level_memory') .'/theme/memory.js');
  }
?><?php
  if ($eg_lvl_memory['debug_mode']) {
?>
<a href="<?php print $eg_lvl_memory['set_played_link']; ?>">flag</a>&nbsp;|&nbsp;
<a href="<?php print $eg_lvl_memory['set_unplayed_link']; ?>">unflag</a>
<?php } ?>
<a id="eg-card-link-<?php print $eg_lvl_memory['nid'];
  ?>" href="<?php print $eg_lvl_memory['question_link'];
  ?>" onclick="EthifyGame.Level.Memory.PlayCard(<?php print $eg_lvl_memory['nid'];
  ?>);" rel="lightframe[][]">
  <span id="eg-card-default-<?php print $eg_lvl_memory['nid']; ?>" class="eg-card<?php if( !$eg_lvl_memory['is_played'] ) print ' eg-card-default-image'; ?>">
  <span id="eg-card-image-<?php print $eg_lvl_memory['nid']; ?>" class="eg-card<?php if( !$eg_lvl_memory['is_played'] ) print ' eg-card-hide'; ?>" style="background-image:url('<?php print $eg_lvl_memory['solved_image_path']; ?>');">
</span>
</span>
</a>
<input type="hidden" name="eg-card-solved-<?php print $eg_lvl_memory['nid']; ?>-done-link" value="<?php print $eg_lvl_memory['btn_done_link']; ?>" />
<input type="hidden" name="eg-card-solved-<?php print $eg_lvl_memory['nid']; ?>-todo-link" value="<?php print $eg_lvl_memory['btn_todo_link']; ?>" />
<input type="hidden" name="eg-card-solved-<?php print $eg_lvl_memory['nid']; ?>-ignore-link" value="<?php print $eg_lvl_memory['btn_ignore_link']; ?>" />
<?php
  if ($eg_lvl_memory['setup']) { ?>
<input type="hidden" name="eg-card-level-prev-link" value="<?php print $eg_lvl_memory['level_prev_link']; ?>" />
<input type="hidden" name="eg-card-level-next-link" value="<?php print $eg_lvl_memory['level_next_link']; ?>" />
<input type="hidden" name="eg-card-nav-prev" value="<?php print $eg_lvl_memory['level_prev_link_show']; ?>" />
<input type="hidden" name="eg-card-nav-next" value="<?php print $eg_lvl_memory['level_next_link_show']; ?>" />
<?php
  }
