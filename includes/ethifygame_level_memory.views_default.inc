<?php
// $Id$
/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function ethifygame_level_memory_views_default_views() {
  $dir = drupal_get_path('module', 'ethifygame_level_memory') .'/views';
  
  include_once $dir . '/ethifygame_level_memory.inc';
  include_once $dir . '/node_%_crop.inc';
  
  $views = array();
  
  $view = ethifygame_level_memory_get_view_memory();
  $views[$view->name] = $view;
  
  $view = ethifygame_level_memory_get_view_siglenode();
  $views[$view->name] = $view;
  
  return $views;
}
